import { boot } from 'quasar/wrappers'

// Ensure unmount is called on page close / refresh
export default boot(({ app }) => {
  window.addEventListener('beforeunload', () => {
    app.unmount()
  })
})

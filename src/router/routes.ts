import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  { path: '/popup', component: () => import('pages/PopupPage.vue') },
  { path: '/capture/:tabId', props: true, component: () => import('pages/CapturePage.vue') },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: 'batch', component: () => import('pages/BatchesPage.vue') },
      { path: 'batch/:id', props: true, component: () => import('pages/BatchPage.vue') },
      { path: 'display/:id', props: true, component: () => import('pages/DisplayPage.vue') },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;

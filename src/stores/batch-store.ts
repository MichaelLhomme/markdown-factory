import { inject, onBeforeUnmount, onMounted, provide, ref } from 'vue'
import { defineStore } from 'pinia'

import { BatchDoc, BatchStub } from 'app/src-bex/controllers/batch-controller'
import { useBatches } from 'src/utils/batches'
import { withLoading } from 'src/utils/loading'

const BATCH_STORE_KEY = 'batch-stores'

const batchStore = defineStore('batch', () => {
  const { listBatches, deleteBatch, saveBatch, createBatch } = useBatches()

  const loading = ref(false)
  const batches = ref<BatchDoc[]>([])

  const init = (autoload?: boolean) => {
    batches.value = []
    if(autoload === true)
      refresh()
  }

  const refresh = () => withLoading(loading, listBatches()
    .then(res => batches.value = res)
    .catch(err => console.error('Error refreshing stores', err))
  )

  const addBatch = (batch: BatchStub) => createBatch(batch)
    .then(createdBatch => {
      batches.value = [ ...batches.value, createdBatch ]
      return createdBatch
    })

  const removeBatch = (batchDoc: BatchDoc) => deleteBatch(batchDoc)
    .then(deleted => {
      batches.value = batches
        .value
        .filter(c => c.id !== deleted.id)

      return deleted
    })

  const updateBatch = (captureDoc: BatchStub) => saveBatch(captureDoc)
    .then(updated => {
      batches.value = batches
        .value
        .map(c => c.id === updated.id ? updated : c)

      return updated
    })


  return {
    loading,
    batches,
    init,
    refresh,
    addBatch,
    removeBatch,
    updateBatch
  }
})

export type BatchesStore = ReturnType<typeof useBatchesStore>

export const reuseBatchesStore = () => inject(BATCH_STORE_KEY) as BatchesStore

export const useBatchesStore = (autoload?: boolean) => {
  const store = batchStore()

  onMounted(() => store.init(autoload))
  provide(BATCH_STORE_KEY, store)
  onBeforeUnmount(() => store.$dispose())

  return store
}

import { inject, onBeforeUnmount, onMounted, provide, ref } from 'vue'
import { defineStore } from 'pinia'

import { TabId } from 'src/utils/tabs'
import { useCaptures } from 'src/utils/captures'
import { BatchFull, BatchId, BatchEvents as BatchEvents, withBatchEventsListener } from 'app/src-bex/controllers/batch-controller'
import { CaptureDoc, CaptureId, CaptureStub } from 'app/src-bex/controllers/capture-controller'
import { withLoading } from 'src/utils/loading'
import { useBatches } from 'src/utils/batches'
import { useBridge } from 'src/utils/bridge'

const CAPTURE_STORE_KEY = 'capture-stores'

const captureStore = defineStore('capture', () => {
  const bridge = useBridge()
  const { getBatch } = useBatches()
  const { listCaptures, createCapture, recreateCapture, saveCapture, deleteCapture } = useCaptures()

  let currentBatchId: BatchId | undefined
  const loading = ref(false)
  const batch = ref<BatchFull>()
  const captures = ref<CaptureDoc[]>([])

  let cleanListeners = () => {;}


  onMounted(() => {
    cleanListeners = bridge.initListeners<BatchEvents>(withBatchEventsListener, {
      onNewCapture: (capture) => captures.value = [ ...captures.value, capture ],
      onProcessingUpdated: (processing) => updateBatchState(processing)
    })
  })

  onBeforeUnmount(() => cleanListeners())


  const init = (batchId: BatchId, autoload?: boolean) => {
    currentBatchId = batchId
    captures.value = []
    if(autoload === true)
      refresh()
  }


  const refreshBatch = () => {
    if(currentBatchId === undefined) throw new Error('CapturesStore is not initialized')

    return withLoading(loading, getBatch(currentBatchId)
      .then(b => batch.value = b)
      .catch(err => console.error('Error refreshing captures', err))
    )
  }


  const updateBatchState = (processing: boolean) => {
    if(batch.value === undefined) return
    batch.value = { ...batch.value, processing }
  }


  const refresh = () => {
    if(currentBatchId === undefined) throw new Error('CapturesStore is not initialized')

    return withLoading(loading, getBatch(currentBatchId)
      .then(b => {
        batch.value = b
        return listCaptures(b.id)
      })
      .then(res => captures.value = res)
      .catch(err => console.error('Error refreshing captures', err))
    )
  }


  const removeCapture = (captureDoc: CaptureDoc) => deleteCapture(captureDoc)
    .then(deleted => {
      captures.value = captures
        .value
        .filter(c => c.id !== deleted.id)

      return deleted
    })


  const updateCapture = (captureDoc: CaptureStub) => saveCapture(captureDoc)
    .then(updated => {
      captures.value = captures
        .value
        .map(c => c.id === updated.id ? updated : c)

      return updated
    })


  const retakeCapture = (captureId: CaptureId) => recreateCapture(captureId)
    .then(updated => {
      captures.value = captures
        .value
        .map(c => c.id === updated.id ? updated : c)

      return updated
    })


  const captureTab = (tabId: TabId) => {
    if(currentBatchId === undefined) throw new Error('CapturesStore is not initialized')

    return createCapture([currentBatchId, tabId])
      .then(added => captures.value = [ ...captures.value, added])
  }


  const isResolvedUrl = (url: string): boolean => captures
    .value
    .some(capture => capture.urls.includes(url))


  const addAcceptedUrl = (url: string) => batch.value?.acceptedUrls.push(url)


  const addRejectedUrl = (url: string) => batch.value?.rejectedUrls.push(url)


  return {
    batch,
    loading,
    captures,
    init,
    refresh,
    refreshBatch,
    captureTab,
    retakeCapture,
    updateCapture,
    removeCapture,
    isResolvedUrl,
    addAcceptedUrl,
    addRejectedUrl
  }
})


export type CapturesStore = ReturnType<typeof captureStore>

export const reuseCapturesStore = () => inject(CAPTURE_STORE_KEY) as CapturesStore

export const useCapturesStore = (batchId: BatchId, autoload?: boolean): CapturesStore => {
  const store = captureStore()
  provide(CAPTURE_STORE_KEY, store)
  onMounted(() => store.init(batchId, autoload))
  onBeforeUnmount(() => store.$dispose())
  return store
}

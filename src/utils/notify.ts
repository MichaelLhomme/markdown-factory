import { Notify, Dialog } from 'quasar'

import ErrorDialog from 'src/components/ErrorDialog.vue'

export function notifyError(message: string, err: unknown) {
  console.error(err)
  Notify.create({
    message,
    timeout: 3000,
    progress: true,
    type: 'negative',
    actions: [
      { icon: 'info', color: 'white', handler: () => {
        Dialog.create({
          component: ErrorDialog,
          componentProps: { err }
        })
      } }
    ]
  })
}

export function notifySuccess(message: string) {
  Notify.create({
    message,
    timeout: 1000,
    progress: true,
    type: 'positive'
  })
}

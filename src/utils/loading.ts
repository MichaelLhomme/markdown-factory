import { Ref } from 'vue'

export enum LoadingStatus {
  PENDING,
  OK,
  ERROR
}


export function withLoading<T>(ref: Ref<boolean>, promise: Promise<T>): Promise<T> {
  ref.value = true
  return promise
    .finally(() => ref.value = false)
}


export function withManyLoading<T, ID extends string | number>(ref: Ref<Record<ID, boolean>>, id: ID, promise: Promise<T>): Promise<T> {
  ref.value[id] = true
  return promise
    .finally(() => ref.value[id] = false)
}


export function withStatus<T>(ref: Ref<LoadingStatus>, promise: Promise<T>): Promise<T> {
  ref.value = LoadingStatus.PENDING
  return promise
    .then(res => {
      ref.value = LoadingStatus.OK
      return res
    })
    .catch(err => {
      ref.value = LoadingStatus.ERROR
      throw err
    })
}


export function withManyStatus<T, ID extends string | number>(ref: Ref<Record<ID, LoadingStatus>>, id: ID, promise: Promise<T>): Promise<T> {
  ref.value[id] = LoadingStatus.PENDING
  return promise
    .then(res => {
      ref.value[id] = LoadingStatus.OK
      return res
    })
    .catch(err => {
      ref.value[id] = LoadingStatus.ERROR
      throw err
    })
}


export function delay(millis: number): Promise<void> {
  return new Promise(resolve => setTimeout(() => resolve(), millis))
}

import { BridgeClientInitCallback, Controller } from 'app/src-bex/controllers/abstract-controller'
import { BridgeListenerInitCallback, Events, EventsListener, ListenersCleanup } from 'app/src-bex/controllers/events'
import { useQuasar } from 'quasar'

export function useBridge() {
  const bex = useQuasar().bex

  const send = <Param, Result>(name: string, payload?: Param): Promise<Result> => {
    return bex
      .send(name, payload)
      .then(res => {
        if(res.data?._markdown_factory_err)
          throw res.data._markdown_factory_err
        else
          return res.data as Result
      })
  }

  const initListeners = <T extends Events>(initCallback: BridgeListenerInitCallback<T>, listener: EventsListener<T>): ListenersCleanup => initCallback(listener, bex)

  const initBridgeClient = <T extends Controller>(initCallback: BridgeClientInitCallback<T>): T => initCallback(event => (arg) => send(event, arg))


  return {
    bex,
    send,
    initBridgeClient,
    initListeners,
  }
}


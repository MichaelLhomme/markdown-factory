import { onBeforeUnmount, onMounted, ref } from 'vue'
import { BexPayload } from '@quasar/app-vite'
import { useQuasar } from 'quasar'
import { BackgroundProgressEvent, ProgressType } from 'app/src-bex/controllers/abstract-controller'

export enum ProgressStatus {
  PENDING,
  COMPLETE,
  ERROR
}

export interface ProgressItem {
  key: string
  status: ProgressStatus
}

export interface ProgressTask extends ProgressItem {
  items: ProgressItem[]
}

export function useBackgroundProgress() {
  const $q = useQuasar()

  const tasks = ref<ProgressTask[]>([])

  const clearTasks = () => tasks.value = []

  const onTaskStart = (data: BackgroundProgressEvent) => {
    tasks.value.push({ key: data.key, status: ProgressStatus.PENDING, items: [] })
  }

  const onTaskComplete = (data: BackgroundProgressEvent) => {
    const task = tasks.value.find(t => t.key === data.key)
    if(task === undefined) return
    task.status = ProgressStatus.COMPLETE
    if(task.items.some(i => i.status === ProgressStatus.ERROR)) return
    setTimeout(() => tasks.value = tasks.value.filter(t => t.key !== task.key), 1000)
  }

  const onTaskError = (data: BackgroundProgressEvent) => {
    const task = tasks.value.find(t => t.key === data.key)
    if(task) task.status = ProgressStatus.ERROR
  }

  const onItemStart = (data: BackgroundProgressEvent) => {
    const task = tasks.value.find(t => t.key === data.task)
    if(task === undefined) return console.warn('No suck task for item', data)
    task.items.push({ key: data.key, status: ProgressStatus.PENDING })
  }

  const onItemComplete = (data: BackgroundProgressEvent) => {
    const task = tasks.value.find(t => t.key === data.task)
    if(task === undefined) return console.warn('No suck task for item', data)
    const item = task.items.find(i => i.key === data.key)
    if(item) item.status = ProgressStatus.COMPLETE
  }

  const onItemError = (data: BackgroundProgressEvent) => {
    const task = tasks.value.find(t => t.key === data.task)
    if(task === undefined) return console.warn('No suck task for item', data)
    const item = task.items.find(i => i.key === data.key)
    if(item) item.status = ProgressStatus.ERROR
  }

  const handleProgress = (ev: BexPayload<BackgroundProgressEvent, unknown>) => {
    switch(ev.data.type) {
      case ProgressType.TASK_START: return onTaskStart(ev.data)
      case ProgressType.TASK_COMPLETE: return onTaskComplete(ev.data)
      case ProgressType.TASK_ERROR: return onTaskError(ev.data)
      case ProgressType.ITEM_START: return onItemStart(ev.data)
      case ProgressType.ITEM_COMPLETE: return onItemComplete(ev.data)
      case ProgressType.ITEM_ERROR: return onItemError(ev.data)
    }
  }

  onMounted(() => $q.bex.on('progress', handleProgress))
  onBeforeUnmount(() => $q.bex.off('progress', handleProgress))

  return {
    tasks,
    clearTasks
  }
}

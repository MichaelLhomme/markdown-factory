import { Attachment } from 'app/src-bex/controllers/abstract-controller'
import { onBeforeUnmount, ref } from 'vue'

export type BlobId = string
export type Blobs = Record<BlobId, string>

export const useBlobHelper = () => {
  const blobs = ref<Blobs>({})

  const blobExists = (id: BlobId): boolean => blobs.value[id] !== undefined

  const addBlob = (id: BlobId, blob: Blob): void => {
    const url = URL.createObjectURL(blob)
    blobs.value[id] = url
  }

  const removeBlob = (id: BlobId): void => {
    URL.revokeObjectURL(blobs.value[id])
    delete blobs.value[id]
  }

  const getBlob = (id: BlobId): Promise<Blob> => {
    if(blobs.value[id] === undefined) throw new Error(`No such blob ${id}`)
    return fetch(blobs.value[id])
      .then(res => res.blob())
  }

  const getBlobAsText = (id: BlobId): Promise<string> => {
    return getBlob(id)
      .then(blob => blob.text())
  }

  const getBlobAsJson = <T>(id: BlobId): Promise<T> => {
    return getBlobAsText(id)
      .then(text => JSON.parse(text) as T)
  }

  const clearBlobs = (): void => {
    Object
      .keys(blobs.value)
      .forEach(removeBlob)

    blobs.value = {}
  }

  const addBlobFromAttachment = (attachment: Attachment): Promise<void> => {
    return fetch(attachment.base64)
      .then(res => res.blob())
      .then(blob => addBlob(attachment.uuid, blob))
  }

  const addBlobsFromAttachments = (attachments: Attachment[]): Promise<PromiseSettledResult<void>[]> => {
    return Promise.allSettled(attachments.map(addBlobFromAttachment))
  }

  onBeforeUnmount(clearBlobs)

  return {
    blobs,
    getBlob,
    getBlobAsText,
    getBlobAsJson,
    blobExists,
    addBlob,
    removeBlob,
    clearBlobs,
    addBlobFromAttachment,
    addBlobsFromAttachments
  }
}

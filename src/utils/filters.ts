export function containsString(a: string, b: string) {
  return a.toLowerCase().includes(b.toLocaleLowerCase())
}

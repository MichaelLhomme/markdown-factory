import { ref } from 'vue'

import { useBlobHelper } from './blobs'
import { Article } from 'src/models/article'
import { ARTICLE_PREVIEW, CAPTURE_TAB, DELETE_ARTICLE, GET_ATTACHMENTS, LIST_ARTICLES, READ_ARTICLE, SAVE_ARTICLE } from 'app/src-bex/controllers/controller'
import { PouchArticle } from 'app/src-bex/utils/pouchdb'
import { Attachment } from 'app/src-bex/controllers/abstract-controller'
import { useBridge } from './bridge'

export type { PouchArticle } from 'app/src-bex/utils/pouchdb'


export function useArticles() {
  const { send } = useBridge()

  const listArticles = (): Promise<PouchArticle[]> => send(LIST_ARTICLES)
  const articleToHtml = (article: Article): Promise<string> => send(ARTICLE_PREVIEW, article)
  const deleteArticle = (id: string, rev: string): Promise<void> => send(DELETE_ARTICLE, { id, rev })
  const saveArticle = (article: Article): Promise<void> => send(SAVE_ARTICLE, article)
  const getAttachments = (docId: string, attachmentIds: string[]): Promise<Attachment[]> => send(GET_ATTACHMENTS, { id: docId, attachmentIds })

  return {
    listArticles,
    articleToHtml,
    saveArticle,
    deleteArticle,
    getAttachments
  }
}


export function useArticleCapture() {
  const { send } = useBridge()

  const article = ref<Article>()
  const htmlBlobUrl = ref<string>()

  const captureTab = (tabId: number): Promise<Article> => send<number, Article>(CAPTURE_TAB, tabId)
      .then(a => article.value = a)

  return {
    captureTab,
    article,
    htmlBlobUrl
  }
}


export function useReadArticle() {
  const { send } = useBridge()

  const article = ref<Article>()
  const { blobs, getBlobAsText, getBlobAsJson, addBlobsFromAttachments } = useBlobHelper()

  const readArticle = (docId: string): Promise<Article> => {
    return send<string, Attachment[]>(READ_ARTICLE, docId)
      .then(attachments => addBlobsFromAttachments(attachments)
        .then(() => getBlobAsText('markdown'))
        .then(markdown => getBlobAsJson<PouchArticle>('article')
          .then(a => article.value = { ...a, images: blobs.value, markdown })
        )
      )
    }

  return {
    article,
    readArticle
  }
}

export type TabId = number

export interface Tab {
  id: TabId,
  url?: string
  title?: string,
  favicon?: string,
}


function toInternalTab(tab: chrome.tabs.Tab): Tab {
  if(!tab.id) throw new Error('Tab got no ID !')
  return {
    id: tab.id,
    url: tab.url,
    title: tab.title,
    favicon: tab.favIconUrl,
  }
}


export function useTabs() {

  const closeTab = (tabId: TabId): Promise<void> => {
    return chrome
      .tabs
      .remove(tabId)
  }

  const currentTab = (): Promise<Tab> => {
    return chrome
      .tabs
      .query({ active: true, lastFocusedWindow: true })
      .then((tabs: chrome.tabs.Tab[]) => {
        if(tabs.length === 0) throw new Error('No active tab')
        return toInternalTab(tabs[0])
      })
  }

  const listTabs = (): Promise<Tab[]> => {
    return chrome
      .tabs
      .query({ })
      .then((tabs: chrome.tabs.Tab[]) => tabs.map(toInternalTab))
  }

  return {
    currentTab,
    closeTab,
    listTabs,
  }
}

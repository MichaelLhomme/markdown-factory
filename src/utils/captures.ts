import { withCaptureBridgeClient } from 'app/src-bex/controllers/capture-controller'
import { useBridge } from './bridge'

export function useCaptures() {
  const { initBridgeClient } = useBridge()
  return initBridgeClient(withCaptureBridgeClient)
}


import { withBatchBridgeClient } from 'app/src-bex/controllers/batch-controller'
import { useBridge } from './bridge'

export function useBatches() {
  const { initBridgeClient } = useBridge()
  return initBridgeClient(withBatchBridgeClient)
}

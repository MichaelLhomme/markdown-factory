export type Links = Record<string, string>

export type Images = Record<string, string>

export interface ArticleInfo {
  title: string
  excerpt: string
  author: string
  siteName: string
  sourceUrl: string
  hero?: string
}

export interface Article extends ArticleInfo {
  markdown: string
  links?: Links
  images?: Images
}

// Hooks added here have a bridge allowing communication between the Web Page and the BEX Content Script.
// More info: https://quasar.dev/quasar-cli/developing-browser-extensions/dom-hooks
import { bexDom } from 'quasar/wrappers'

// TODO Need to lazy load this in extractTabContent intead of importing in every page
import { Readability } from '@mozilla/readability'

export default bexDom((/* bridge */) => {

  function captureContent() {
    const documentClone = document.cloneNode(true) as Document;
    const readable = new Readability(documentClone).parse();
    if(readable === null || readable === undefined) throw new Error('Error parsing readable version of document')
    return { url: document.URL, html: document.documentElement.outerHTML, readable }
  }

  (window as any).captureContent = captureContent

  /*
  bridge.send('message.to.quasar', {
    worked: true
  })
  */
})

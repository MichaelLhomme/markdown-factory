import PouchDB from 'pouchdb'
import find from 'pouchdb-find'
import rel from 'relational-pouch'
import { v4 as uuidv4 } from 'uuid'

import { Article, ArticleInfo } from 'src/models/article'

import { createSchema, getEntityPlural } from './pouch-schema'


PouchDB
  .plugin(find)
  .plugin(rel)


/**
 * Schema
 */

export enum Entities {
  BATCH = 'batch',
  CAPTURE = 'capture',
}

const schema = createSchema([
  { singular: Entities.BATCH, plural: 'batches' },
  { singular: Entities.CAPTURE, plural: 'captures', relations: [
    { type: 'belongsTo', entityName: Entities.BATCH }
  ] },
])

const db = new PouchDB('markdown-factory')
const relDB = db.setSchema(schema)

export const useDatabase = () => relDB
export const useRelDatabase = () => relDB.rel


/**
 * Types
 */

export type PouchId = string

export interface PouchDocId {
  id: PouchId
  rev: string
}

export type PouchDoc<T> = T & PouchDocId & {
  attachments?: PouchAttachmentStubs
}


export type PouchStub<T> = T & {
  id?: PouchId
  rev?: string
  attachments?: PouchAttachments | PouchAttachmentStubs
}


/**
 * Entities helpers
 */

export function findEntities<T>(entity: Entities, idOrIds?: PouchId | PouchId[]): Promise<T[]> {
  return useRelDatabase()
  .find(entity, idOrIds)
  .then(res => res[getEntityPlural(entity)] as PouchDoc<T>[])
}

export function findEntity<T>(entity: Entities, id: PouchId): Promise<T> {
  return findEntities<T>(entity, id)
  .then(entities => {
    if(entities === undefined) throw new Error('Bad response')
    if(entities.length === 0) throw new Error('No result')
    if(entities.length > 1) throw new Error(`Should only return 1 entity, got ${ entities.length }`)
    return entities[0]
  })
}

export function findEntitiesMatchingId<T>(entity: Entities, idPattern: string): Promise<T[]> {
  return useDatabase()
    .allDocs({
      startkey: idPattern,
      endkey: `${ idPattern }\ufff0`
    })
    .then(res => {
      const relDb = useRelDatabase()
      const ids = res.rows
        .map(r => relDb.parseDocID(r.id))
        .map(relId => relId.id)

      return findEntities<T>(entity, ids)
    })
}

export function saveEntity<T>(entity: Entities, doc: PouchStub<T>): Promise<PouchDocId> {
  return useRelDatabase()
    .save(entity, doc)
}

export function deleteEntity(entity: Entities, doc: PouchDocId): Promise<PouchDocId> {
  return useRelDatabase()
    .del(entity, doc)
    .then(res => {
      if(!res.deleted) throw new Error('Entity not deleted')
      return doc
    })
}

export function deleteEntitiesMatchingId(entity: Entities, idPattern: string): Promise<void> {
  return useDatabase()
    .allDocs({
      startkey: idPattern,
      endkey: `${ idPattern }\ufff0`
    })
    .then(res => {
      const relDb = useRelDatabase()
      const promises = res.rows
        .map(r => {
          const parsedId = relDb.parseDocID(r.id)
          return deleteEntity(entity, { id: parsedId.id, rev: r.value.rev })
        })

        return Promise
        .allSettled(promises)
        .then(res => {
          if(res.some(r => r.status === 'rejected')) throw new Error('Unable to delete some capture')
        })
    })
}



/*
 * Attachments helpers
 */

export type PouchAttachmentId = string

export interface PouchAttachment {
  content_type: string
  data: Blob
}

export type PouchAttachments = Record<PouchAttachmentId, PouchAttachment>

export interface PouchAttachmentStub {
  content_type: string
  digest: string
  length: number
  revpos: number
  stub: true
}

export type PouchAttachmentStubs = Record<PouchAttachmentId, PouchAttachmentStub>


export function saveBlobAsAttachment(blob: Blob): PouchAttachment {
  return { content_type: blob.type, data: blob }
}

export function saveStringAsAttachment(str: string, type: string): PouchAttachment {
  const blob = new Blob([ String(str) ], { type })
  return saveBlobAsAttachment(blob)
}

export function saveObjectAsAttachment(obj: unknown, type: string): PouchAttachment {
  const str = JSON.stringify(obj)
  return saveStringAsAttachment(str, type)
}

export function getAttachments(docId: PouchId): Promise<PouchAttachments> {
  return useDatabase()
    .get(docId, { attachments: true, binary: true })
    .then(doc => {
      if(doc._attachments === undefined) return {}
      return doc._attachments as PouchAttachments
    })
}

export function getAttachment(docId: PouchId, attachmentId: PouchAttachmentId): Promise<PouchAttachment> {
  return useDatabase()
    .getAttachment(docId, attachmentId)
    .then(pouchAttachment => saveBlobAsAttachment(pouchAttachment as Blob))
}



/**
 * TODO Refactor articles using pouch-relational like batch & captures
 */

export interface PouchArticle extends ArticleInfo {
  _id: PouchId,
  _rev: string,
}

interface PouchArticleWithAttachments extends PouchArticle {
  _id: PouchId,
  _rev: string,
  _attachments: PouchAttachments
}

export function pouchDelete(id: PouchId, rev: string) {
  return db
    .remove(id, rev)
}

export function pouchReadArticle(docId: PouchId): Promise<PouchArticleWithAttachments> {
  return db
    .get(docId, { attachments: true, binary: true })
}

export function pouchListArticles(): Promise<PouchArticle[]> {
  return db
    .allDocs({ include_docs: true })
    .then(docs => docs.rows.map(row => row.doc as unknown as PouchArticle))
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function pouchSaveArticle(article: Article, results: any[]) {
  const markdown = new Blob([ String(article.markdown) ], { type: 'text/markdown' })

  const attachments: PouchAttachments = {
    markdown: {
      content_type: markdown.type,
      data: markdown
    }
  }

  results.forEach(res => {
    if(res.status === 'rejected') return
    const uuid = res.value.uuid
    const blob = res.value.blob
    attachments[uuid] = {
      content_type: blob.type,
      data: blob
    }
  })

  let hero: PouchAttachmentId | undefined
  if(article.images) {
    const uuids = Object.keys(article.images)
    hero = uuids.length === 0 ? undefined : uuids[0]
  }

  const db = useDatabase()
  const doc = {
    _id: uuidv4(),
    ...article,
    hero,
    markdown: undefined,
    _attachments: attachments
  }

  return db.put(doc)
}

export function pouchGetAttachment(docId: PouchId, attachmentId: PouchAttachmentId): Promise<Blob> {
  return db
    .getAttachment(docId, attachmentId)
    .then(blobOrBuffer => blobOrBuffer as Blob)
}

export interface PouchEntity {
  singular: string,
  plural: string
  relations?: PouchRelation[]
}

export interface PouchRelation {
  fieldName?: string
  type: 'hasMany' | 'belongsTo'
  entityName: string
}

const entitiesPlural: Record<string, string> = {}


export const createSchema = (entities: PouchEntity[]): object => {
  entities.forEach(e => entitiesPlural[e.singular] = e.plural)
  return entities.map(e => serializeEntity(e))
}


export const getEntityPlural = (entity: string): string => {
  const plural = entitiesPlural[entity]
  if(plural === undefined) throw new Error(`Missing plural for ${ entity }`)
  return plural
}


const serializeRelation = (relation: PouchRelation): object => {
  return {
    [relation.fieldName ?? relation.entityName]: { [relation.type]: relation.entityName }
  }
}


const serializeEntity = (entity: PouchEntity): object => {
  return {
    singular: entity.singular,
    plural: entity.plural,
    relations: entity.relations?.reduce((acc, r) => ({ ...acc, ...serializeRelation(r) }), {})
  }
}


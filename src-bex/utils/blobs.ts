
export function stringToBlob(str: string, type?: string): Promise<Blob> {
  return Promise.resolve(new Blob([ str ], { type: type ?? 'text' }))
}

export function blobToBase64(blob: Blob): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(blob)
    reader.onloadend = () => {
        const base64data = reader.result
        resolve(String(base64data))
    }
    reader.onerror = (err) => reject(err)
  })
}

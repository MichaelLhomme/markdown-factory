import { BexBridge } from '@quasar/app-vite'

let bridge: BexBridge | undefined

export function setupBridge(bexBridge: BexBridge) {
  bridge = bexBridge
}

export function useBridge(): BexBridge {
  if(bridge === undefined) throw new Error('Bridge is not configured')
  return bridge
}

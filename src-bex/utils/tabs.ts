import { ReadableDocument } from './markdown/readability'

export type TabId = number

export interface TabContent {
  url: string
  html: string
  readable: ReadableDocument
}

export interface Tab {
  id: TabId,
  url?: string
  title?: string,
  favicon?: string,
}

function toInternalTab(tab: chrome.tabs.Tab): Tab {
  if(!tab.id) throw new Error('Tab got no ID !')
  return {
    id: tab.id,
    url: tab.url,
    title: tab.title,
    favicon: tab.favIconUrl,
  }
}

export function closeTab(tabId: TabId): Promise<void> {
  return chrome
    .tabs
    .remove(tabId)
}

interface CapturableWindow {
  captureContent: () => TabContent
}

export function extractTabContent(tabId: TabId): Promise<TabContent> {
  // TODO move readability here, need to find a way to lazy load the dependency
  const extractTab = () => (window as unknown as CapturableWindow).captureContent()

  try {
    return chrome
      .scripting
      .executeScript({
        world: 'MAIN',
        func : extractTab,
        target : { tabId },
      })
      .then(results => {
        const [ tabResult ] = results
        return tabResult.result as TabContent
      });
  } catch(err) {
    return Promise.reject(err)
  }
}

const loadHandlers = new Map<TabId, { resolve: (tab: chrome.tabs.Tab) => void, reject: () => void }>()

const onTabUpdated = (tabId: TabId, changeInfo: chrome.tabs.TabChangeInfo, tab: chrome.tabs.Tab) => {
  if(tabId === undefined || loadHandlers.has(tabId) === false) return
  if(changeInfo.status === 'complete') loadHandlers.get(tabId)?.resolve(tab)
}

chrome.tabs.onUpdated.addListener(onTabUpdated)

const waitForComplete = (tabId: TabId) => {
  return new Promise<chrome.tabs.Tab>((resolve, reject) => {
    loadHandlers.set(tabId, { resolve, reject })
  })
}

export const openTabForCapture = (url: string): Promise<Tab> => {
  return chrome
    .tabs
    .create({
      url,
      active: false
    })
    .then(tab => waitForComplete(tab.id as TabId))
    .then(tab => Promise
      .resolve(toInternalTab(tab))
      .finally(() => loadHandlers.delete(tab.id as TabId))
    )
}

import { unified } from 'unified'
import rehypeParse from 'rehype-parse'
import rehypeRemark from 'rehype-remark'
import rehypeFormat from 'rehype-format'
import rehypeDocument from 'rehype-document'
import rehypeHighlight from 'rehype-highlight'
import rehypeStringify from 'rehype-stringify'

import remarkGfm from 'remark-gfm'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype'
import removeComments from 'remark-remove-comments'
import remarkStringify, { Options as RemarkStringifyOptions } from 'remark-stringify'

import updateExternalResources, { Options as UpdateExternalResourcesOptions } from './mdast-update-external-resources'

import { Article } from 'src/models/article'
import { ReadableDocument } from './readability'
import extractExternalResources, { ExtractedResourceFile } from './mdast-extract-external-resources'
import handleLazyImages, { Options as LazyImagesOptions } from './hast-handle-lazy-images'
import listExternalResources, { ListedResourceFile } from './mdast-list-external-resources'
import { CaptureInfo } from 'app/src-bex/controllers/capture-controller'


// See https://github.com/remarkjs/remark/tree/main/packages/remark-stringify
const markdownConfig: RemarkStringifyOptions = {
  bullet: '-',
  fence: '`',
  fences: true,
  incrementListMarker: false
}

export interface DownloadedImage {
  uuid: string
  blob: Blob
}


export async function readableToCapture(sourceUrl: string, doc: ReadableDocument): Promise<{ markdown: string, info: CaptureInfo }> {
  // TODO parse hero from HTML HEAD
  // TODO remove link for images linking to self
  // TODO remove empty link ('' and about:blank)
  return unified()
    .use(rehypeParse)
    .use(handleLazyImages, { properties: [ 'dataLazySrc' ] } as LazyImagesOptions )
    .use(rehypeRemark)
    .use(remarkGfm)
    .use(listExternalResources)
    .use(removeComments)
    .use(remarkStringify, markdownConfig)
    .process(doc.content)
    .then(file => file as ListedResourceFile)
    .then(file => {
      const info: CaptureInfo = {
        title: doc.title,
        excerpt: doc.excerpt,
        author: doc.byline,
        siteName: doc.siteName,
        sourceUrl: sourceUrl,
        hero: file.data.images[0],
        links: file.data.links,
        images: file.data.images,
        files: file.data.files
      }

      return {
        info,
        markdown: String(file),
      }
    })
}


export async function readableToArticle(sourceUrl: string, doc: ReadableDocument): Promise<Article> {
  return unified()
    .use(rehypeParse)
    .use(handleLazyImages, { properties: [ 'dataLazySrc' ] } as LazyImagesOptions )
    .use(rehypeRemark)
    .use(remarkGfm)
    .use(removeComments)
    .use(remarkStringify, markdownConfig)
    .process(doc.content)
    .then(file => {
      const article: Article = {
        title: doc.title,
        excerpt: doc.excerpt,
        author: doc.byline,
        siteName: doc.siteName,
        sourceUrl: sourceUrl,
        markdown: String(file),
      }

      return article
    })
}


export async function markdownToHtml(markdown: string): Promise<string> {
  return unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(remarkRehype)
    .use(rehypeDocument)
    .use(rehypeFormat)
    .use(rehypeHighlight, { detect: true })
    .use(rehypeStringify)
    .process(markdown)
    .then(file => String(file))
}


export async function articleToHtml(article: Article): Promise<string> {
  return unified()
    .use(remarkParse)
    .use(updateExternalResources, { links: article.links, images: article.images } as UpdateExternalResourcesOptions)
    .use(remarkGfm)
    .use(remarkRehype)
    .use(rehypeDocument)
    .use(rehypeFormat)
    .use(rehypeHighlight, { detect: true })
    .use(rehypeStringify)
    .process(article.markdown)
    .then(file => String(file))
}


export async function fetchImage(uuid: string, url: string): Promise<DownloadedImage> {
  return fetch(url)
    .then(res => {
      if(!res.ok) throw new Error(`Error fetching image ${url} : ${res.status} - ${res.statusText}`)
      return res.blob()
    })
    .then(blob => {
      return { uuid, blob }
    })
}


export async function articleToSaveable(article: Article): Promise<ExtractedResourceFile> {
  return unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(extractExternalResources)
    .use(remarkStringify, markdownConfig)
    .process(article.markdown)
    .then(f => f as ExtractedResourceFile)
}


// TODO need it ?
export async function articleToEditableMarkdown(article: Article): Promise<string> {
  return unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(updateExternalResources, { links: article.links, images: article.images } as UpdateExternalResourcesOptions)
    .use(remarkStringify, markdownConfig)
    .process(article.markdown)
    .then(file => String(file))
}

import { HastRoot } from 'rehype-remark/lib'
import { selectAll} from 'hast-util-select'

export interface Options {
  properties: string[]
}

export default function handleLazyImages(options?: Options) {
  const opts = options ?? { properties: [] }

  return (root: HastRoot) => {
    selectAll('img', root).forEach(img => {
      if(!img.properties) return

      const useProperty = Object
        .keys(img.properties)
        .find(property => opts.properties.includes(property))

      if(useProperty) {
        img.properties['src'] = img.properties[useProperty]
      }
    });
  }
}

import { Node } from 'unist'
import { Link, Image } from 'mdast'

export function withLink(node: Node, callback: (link: Link) => void): void {
  if(node.type === 'link') callback(node as Link)
}

export function withImage(node: Node, callback: (image: Image) => void): void {
  if(node.type === 'image') callback(node as Image)
}

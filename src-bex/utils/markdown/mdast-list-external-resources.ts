import { visit } from 'unist-util-visit'
import { MdastRoot } from 'rehype-remark/lib'
import { Node as UnistNode } from 'unist'
import { VFile, Data } from 'vfile'
import { withImage, withLink } from './mdast'

const HTML_EXTENSIONS = ['html', 'htm']

export type ListedResourceFile = VFile & {
  data: Data & {
    links: string[],
    images: string[],
    files: string[]
  }
}

export default function listExternalResources() {
  return (ast: MdastRoot, file: ListedResourceFile) => {
    file.data.links = []
    file.data.images = []
    file.data.files = []

    const pathIsFile = (path: string): boolean => {
      const parts = path.split('/')
      const filename = parts[parts.length - 1]
      const fileParts = filename.split('.')
      if(fileParts.length <= 1) return false

      const ext = fileParts[fileParts.length - 1]
      return !HTML_EXTENSIONS.includes(ext.toLowerCase())
    }

    const isFile = (url: string): boolean => {
      if(url.startsWith('#')) return false
      else if(url.startsWith('mailto')) return false
      else if(url.startsWith('http')) {
        try {
          const u = new URL(url)
          return pathIsFile(u.pathname)
        } catch(err) {
          console.warn('Error testing if url is a file', url)
          return false
        }
      } else {
        return pathIsFile(url)
      }
    }

    visit(ast, ['link', 'image'], (node: UnistNode) => {
      // Extract link URL
      withLink(node, link => {
        if(isFile(link.url))
          file.data.files.push(link.url)
        else
          file.data.links.push(link.url)
      })

      // Extract image url
      withImage(node, image => {
        file.data.images.push(image.url)
      })
    })
  }
}


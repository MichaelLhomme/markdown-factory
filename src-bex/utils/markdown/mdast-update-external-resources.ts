import { visit } from 'unist-util-visit'
import { MdastRoot } from 'rehype-remark/lib'
import { Node as UnistNode } from 'unist'
import { withImage, withLink } from './mdast'

export interface Options {
  links?: Record<string, string>,
  images?: Record<string, string>
}

export default function updateExternalResources(options?: Options) {
  return (ast: MdastRoot) => {
    visit(ast, ['link', 'image'], (node: UnistNode) => {
      // Update links
      withLink(node, link => {
        if(options?.links)
          link.url = options.links[link.url] ?? link.url
      })

      // Update images
      options?.images && withImage(node, image => {
        if(options?.images)
          image.url = options.images[image.url] ?? image.url
      })
    })
  }
}

import { visit } from 'unist-util-visit'
import { MdastRoot } from 'rehype-remark/lib'
import { VFile, Data } from 'vfile'
import { v4 as uuidv4 } from 'uuid'
import { Node as UnistNode } from 'unist'
import { withImage, withLink } from './mdast'

export type ExtractedResourceFile = VFile & {
  data: Data & {
    links: Record<string, string>,
    images: Record<string, string>
  }
}

export default function extractExternalResources() {
  return (ast: MdastRoot, file: ExtractedResourceFile) => {
    file.data.links = {}
    file.data.images = {}

    visit(ast, ['link', 'image'], (node: UnistNode) => {
      // Extract link URL
      withLink(node, link => {
        const id = uuidv4()
        const url = link.url
        link.url = id
        file.data.links[id] = url
      })

      // Extract image url
      withImage(node, image => {
        const id = uuidv4()
        const url = image.url
        image.url = id
        file.data.images[id] = url
      })
    })
  }
}

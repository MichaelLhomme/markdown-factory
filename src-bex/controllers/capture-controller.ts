import { markdownToHtml, readableToCapture } from '../utils/markdown/articles'
import { TabId, closeTab, extractTabContent, openTabForCapture } from '../utils/tabs'
import { Attachment, Controller, initControllerClient, pouchAttachmentsToAttachments, registerController, stringToAttachment } from './abstract-controller'
import { Entities, PouchDoc, PouchDocId, PouchId, PouchStub, deleteEntity, findEntitiesMatchingId, findEntity, getAttachments, saveEntity, saveStringAsAttachment, useRelDatabase } from '../utils/pouchdb'
import { v4 as uuidv4 } from 'uuid'
import { BatchId } from './batch-controller'


export type CaptureId = PouchId

export interface CaptureInfo {
  title: string
  excerpt: string
  author: string
  siteName: string
  sourceUrl: string
  hero?: string
  links: string[]
  images: string[]
  files: string[]
}

export type Capture = {
  batch: BatchId
  urls: string[]
  info: CaptureInfo
  capturedAt: Date
}

export type CaptureDoc = PouchDoc<Capture>
export type CaptureStub = PouchStub<Capture>

export interface ICaptureController extends Controller {
  listCaptures: (batchId: BatchId) => Promise<CaptureDoc[]>
  deleteCapture: (doc: PouchDocId) => Promise<PouchDocId>
  saveCapture: (capture: CaptureStub) => Promise<CaptureDoc>
  addUrlToCapture: ([captureId, url]: [ CaptureId, string ]) => Promise<PouchDocId>
  previewCapture: (captureId: CaptureId) => Promise<Attachment[]>
  createCapture: ([batchId, tabId]: [ BatchId, TabId]) => Promise<CaptureDoc>
  recreateCapture: (captureId: CaptureId) => Promise<CaptureDoc>
}



export const findCapture = (id: CaptureId) => findEntity<CaptureDoc>(Entities.CAPTURE, id)
export const _saveCapture = (capture: CaptureStub) => saveEntity(Entities.CAPTURE, capture)
export const deleteCapture = (docId: PouchDocId) => deleteEntity(Entities.CAPTURE, docId)


export const findCapturesByBatch = (batchId: BatchId): Promise<CaptureDoc[]> => {
    const idPattern = generateCaptureIdPattern(batchId)
    return findEntitiesMatchingId(Entities.CAPTURE, idPattern)
  }


export const generateCaptureId = (batchId: BatchId, captureId?: CaptureId): string => {
  return captureId ? `${Entities.BATCH}-${ batchId }-${ captureId }` : `${Entities.BATCH}-${ batchId }`
}


export const generateCaptureIdPattern = (batchId: BatchId): string => useRelDatabase()
  .makeDocID({
    type: Entities.CAPTURE,
    id: generateCaptureId(batchId)
  })


export const saveCapture = (capture: CaptureStub): Promise<CaptureDoc> => _saveCapture(capture)
  .then(res => findCapture(res.id))


export const addUrlToCapture = (captureId: CaptureId, url: string): Promise<PouchDocId> => findCapture(captureId)
  .then(capture => {
    if(capture.urls.includes(url)) return Promise.resolve({ id: capture.id, rev: capture.rev })
    return saveCapture({ ...capture, urls: [ ...capture.urls, url ]})
  })


export const previewCapture = (captureId: CaptureId): Promise<Attachment[]> => {
  const pouchId = useRelDatabase()
    .makeDocID({
      id: captureId,
      type: Entities.CAPTURE
    })

  return getAttachments(pouchId)
    .then(pouchAttachments => pouchAttachmentsToAttachments(pouchAttachments)
        .then(attachments => {
          const markdownBlob = pouchAttachments['markdown']
          if(markdownBlob === undefined) return attachments
          return markdownBlob
            .data
            .text()
            .then(md => markdownToHtml(md))
            .then(htmlPreview => stringToAttachment(htmlPreview, 'preview'))
            .then(htmlAttachment => [ ...attachments, htmlAttachment ])
        })
    )
}


export const createCapture = (batchId: BatchId, tabId: TabId): Promise<CaptureDoc> => captureTab(batchId, tabId)
  .then(capture => saveCapture(capture))
  .then(res => findCapture(res.id))


export const captureTab = (batchId: BatchId, tabId: TabId): Promise<CaptureStub> => extractTabContent(tabId)
  .then(tabContent => readableToCapture(tabContent.url, tabContent.readable)
    .then(tabCapture => {
      const capture: CaptureStub = {
        id: generateCaptureId(batchId, uuidv4()),
        batch: batchId,
        urls: [ tabContent.url ],
        info: tabCapture.info,
        capturedAt: new Date(),
        attachments: {
          html: saveStringAsAttachment(tabContent.html, 'text/html'),
          markdown: saveStringAsAttachment(tabCapture.markdown, 'text/markdown')
        }
      }

      return capture
    })
  )


export const recreateCapture = (captureId: CaptureId): Promise<CaptureDoc> => findCapture(captureId)
  .then(capture => openTabForCapture(capture.info.sourceUrl)
    .then(tab => captureTab(capture.batch, tab.id)
      .then(newCapture => saveCapture({ ...newCapture, id: capture.id, rev: capture.rev }))
      .finally(() => closeTab(tab.id))
    )
  )
    .then(() => findCapture(captureId))


const controller: ICaptureController = {
  deleteCapture,
  saveCapture,
  previewCapture,
  recreateCapture,
  listCaptures: findCapturesByBatch,
  addUrlToCapture: ([captureId, url]) => addUrlToCapture(captureId, url),
  createCapture: ([batchId, tabId]) => createCapture(batchId, tabId),
}


const CONTROLLER_NAME = 'capture'
export const registerCaptureController = () => registerController(CONTROLLER_NAME, controller)
export const withCaptureBridgeClient = initControllerClient(CONTROLLER_NAME, controller)

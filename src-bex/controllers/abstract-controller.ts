import { BexBridge, BexPayload } from '@quasar/app-vite'
import { blobToBase64, stringToBlob } from '../utils/blobs'
import { PouchAttachments } from '../utils/pouchdb'
import { useBridge } from '../utils/bridge'


export type TaskKey = string

export type ItemKey = string

export enum ProgressType {
  TASK_START,
  TASK_COMPLETE,
  TASK_ERROR,
  ITEM_START,
  ITEM_COMPLETE,
  ITEM_ERROR
}

export interface BackgroundProgressEvent {
  type: ProgressType
  key: string
  task: TaskKey
}

export interface Attachment {
  uuid: string
  base64: string
}


/*
 * Attachments helpers
 */

export function stringToAttachment(str: string, uuid: string, type?: string): Promise<Attachment> {
  return Promise
    .resolve(str)
    .then(s => stringToBlob(s, type))
    .then(blob => blobToAttachment(blob, uuid))
}

export function blobToAttachment(blob: Blob, uuid: string): Promise<Attachment> {
  return Promise
    .resolve(blob)
    .then(blobToBase64)
    .then(base64 => ({ uuid, base64 }))
}

export function objectToAttachment(obj: unknown, uuid: string): Promise<Attachment> {
  return Promise
    .resolve(obj)
    .then(JSON.stringify)
    .then(stringToBlob)
    .then(blobToBase64)
    .then(base64 => ({ uuid, base64 }))
}

export function pouchAttachmentsToAttachments(pouchAttachments: PouchAttachments): Promise<Attachment[]> {
  const promises = Object
    .entries(pouchAttachments)
    .map(entry => blobToAttachment(entry[1].data, entry[0]))

  return Promise
    .allSettled(promises)
    .then(result => {
      return result
        .filter(r => r.status === 'fulfilled')
        .map(r => r as PromiseFulfilledResult<Attachment>)
        .map(r => r.value)
    })
}


/*
 * Bridge helpers
 */

export type HandlerCallback<TData, TResponse> = (data: TData) => Promise<TResponse>
export function register<TData, TResponse>(event: string, callback: HandlerCallback<TData, TResponse>): void {
  useBridge().on(event, (ev: BexPayload<TData, unknown>) => {
    callback(ev.data)
      .then(res => ev.respond(res))
      .catch(err => {
        console.warn('Handling background error', err)
        ev.respond({ _markdown_factory_err: `${err}` })
      })
  })
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Controller = Record<string, HandlerCallback<any, any>>

export function registerController<T extends Controller>(name: string, controller: T) {
  Object
    .entries(controller)
    .map(entry => ({ event: `${name}-${entry[0]}`, callback: entry[1] }))
    .forEach(({ event, callback }) => register(event, callback))
}

export type BridgeClientInitCallback<T extends Controller> = (callback: (event: string) => HandlerCallback<unknown, unknown>) => T
export const initControllerClient = <T extends Controller>(name: string, controller: T): BridgeClientInitCallback<T> =>
  (callback) => withControllerClient(name, controller, callback)
export function withControllerClient<T extends Controller>(name: string, controller: T, callback: (event: string) => HandlerCallback<unknown, unknown>): T {
  return Object
    .entries(controller)
    .map(entry => ({ id: entry[0], event: `${name}-${entry[0]}` }))
    .reduce((acc, item) => {
      acc[item.id] = callback(item.event)
      return acc
    }, {} as Controller) as T
}

/**
 * Abstract class for bex
 * TODO remove once Articles are refactored to new API
 */
export abstract class AbstractController {
  protected bex: BexBridge

  constructor(bex: BexBridge) {
    this.bex = bex
    this.setupHandlers()
  }

  protected abstract setupHandlers(): void

  protected register<TData, TResponse>(name: string, callback: HandlerCallback<TData, TResponse>) {
    this.bex.on(name, (ev: BexPayload<TData, TResponse>) => {
      callback(ev.data)
        .then(res => ev.respond(res)) // TODO fix typing ?
        .catch(err => {
          console.warn('Handling background error', err)
          ev.respond({ _markdown_factory_err: `${err}` }) // TODO fix typing ?
        })
    })
  }

  protected withTask<T>(task: ItemKey, callback: (task: TaskKey) => Promise<T>): Promise<T> {
    this.taskStart(task)
    return Promise
      .resolve()
      .then(() => callback(task))
      .then(res => {
        this.taskComplete(task)
        return res
      })
      .catch(err => {
        this.taskError(task)
        throw err
      })
  }

  protected withItem<T>(task: TaskKey, key: ItemKey, callback: () => Promise<T>): Promise<T> {
    this.itemStart(task, key)
    return callback()
      .then(res => {
        this.itemComplete(task, key)
        return res
      })
      .catch(err => {
        this.itemError(task, key)
        throw err
      })
  }

  protected sendProgress = (e: BackgroundProgressEvent) => this.bex.send('progress', e)

  protected taskStart = (task: TaskKey) => this.sendProgress({ type: ProgressType.TASK_START, key: task, task })
  protected taskComplete = (task: TaskKey) => this.sendProgress({ type: ProgressType.TASK_COMPLETE, key: task, task })
  protected taskError = (task: TaskKey) => this.sendProgress({ type: ProgressType.TASK_ERROR, key: task, task })

  protected itemStart = (task: TaskKey, key: ItemKey) => this.sendProgress({ type: ProgressType.ITEM_START, key, task })
  protected itemComplete = (task: TaskKey, key: ItemKey) => this.sendProgress({ type: ProgressType.ITEM_COMPLETE, key, task })
  protected itemError = (task: TaskKey, key: ItemKey) => this.sendProgress({ type: ProgressType.ITEM_ERROR, key, task })
}

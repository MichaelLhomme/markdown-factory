import { BexBridge, BexEventListener, BexPayload } from '@quasar/app-vite'
import { useBridge } from '../utils/bridge'


// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Events = Record<string, any>

export type EventConsumer<T> = (ev: T) => void

export type EventsPlaceholder<Type> = {
  [Property in keyof Type]: true
}

export type EventsEmitter<Type> = {
  [Property in keyof Type]: EventConsumer<Type[Property]>
}

export type EventsListener<Type> = {
  [Property in keyof Type as `on${Capitalize<string & Property>}`]: EventConsumer<Type[Property]>
}


export function createEventsEmitter<T extends Events>(name: string, events: EventsPlaceholder<T>): EventsEmitter<T> {
  const bridge = useBridge()

  return Object
    .keys(events)
    .reduce((acc, e) => {
      const eventName = `${name}-on${e[0].toUpperCase()}${e.slice(1)}`
      acc[e] = (arg: unknown) => bridge.send(eventName, arg)
      return acc
    }, {} as Events) as EventsEmitter<T>
}

export type ListenersCleanup = () => void
export type BexCallback = (id: string, listener: BexEventListener<string>) => void
export type BridgeListenerInitCallback<T extends Events> = (listener: EventsListener<T>, clientBridge: BexBridge) => ListenersCleanup
export const initEventsListener = <T extends Events>(name: string): BridgeListenerInitCallback<T> =>
  (listener, clientBridge) => withEventsListener<T>(name, listener, clientBridge)
export function withEventsListener<T extends Events>(name: string, listener: EventsListener<T>, clientBridge: BexBridge): ListenersCleanup {
  const bexListeners = Object
    .entries(listener)
    .reduce((acc, [id, callback]) => {
      const eventName = `${name}-${id}`
      const l = createListener(eventName, callback)
      acc[eventName] = l
      clientBridge.on(eventName, l)
      return acc
    }, {} as Record<string, BexEventListener<string>>)

  return () => Object
    .entries(bexListeners)
    .map(([id, l]) => clientBridge.off(id, l))
}


function createListener<T>(event: string, callback: EventConsumer<T>) {
  return (ev: BexPayload<T, unknown>) => {
    try {
      callback(ev.data)
    } catch(err) {
      console.error(`Error processing bex event ${event}`, err)
    }
  }
}

import { Article } from 'src/models/article'
import { DownloadedImage, articleToHtml, articleToSaveable, fetchImage, readableToArticle } from '../utils/markdown/articles'
import { TabId, extractTabContent } from '../utils/tabs'
import { AbstractController, Attachment, blobToAttachment, objectToAttachment } from './abstract-controller'

import { PouchArticle, pouchDelete, pouchGetAttachment as pouchGetAttachments, pouchListArticles, pouchReadArticle, pouchSaveArticle } from '../utils/pouchdb'


export const CAPTURE_TAB = 'capture-tab'
export const ARTICLE_PREVIEW = 'article-preview'
export const READ_ARTICLE = 'read-article'
export const SAVE_ARTICLE = 'save-article'
export const LIST_ARTICLES = 'list-articles'
export const DELETE_ARTICLE = 'delete-article'
export const GET_ATTACHMENTS = 'get-attachments'


// TODO refactor with new API
export class Controller extends AbstractController {

  setupHandlers(): void {
    this.register(CAPTURE_TAB, (tabId: number) => this.captureTab(tabId))
    this.register(ARTICLE_PREVIEW, (article: Article) => this.articlePreview(article))
    this.register(SAVE_ARTICLE, (article: Article) => this.saveArticle(article))
    this.register(READ_ARTICLE, (docId: string) => this.readArticle(docId))
    this.register(LIST_ARTICLES, () => this.listArticles())
    this.register(DELETE_ARTICLE, ({ id, rev }: { id: string, rev: string }) => this.deleteArticle(id, rev))
    this.register(GET_ATTACHMENTS, ({ id, attachmentIds }: { id: string, attachmentIds: string[] }) => this.getAttachments(id, attachmentIds))
  }


  listArticles(): Promise<PouchArticle[]> {
    return this.withTask('load-articles', (task) => Promise
      .resolve()
      .then(() => this.withItem(task, 'dblist', () => pouchListArticles()))
    )
  }


  captureTab(tabId: TabId): Promise<Article> {
    return this.withTask(`capture-tab-${tabId}`, (task) => Promise
      .resolve(tabId)
      .then(id => this.withItem(task, 'extract', () => extractTabContent(id)))
      .then(data => this.withItem(task, 'convert', () => readableToArticle(data.url, data.readable)))
    )
  }


  articlePreview(article: Article): Promise<string> {
    return this.withTask('article-preview', (task) => Promise
      .resolve(article)
      .then(id => this.withItem(task, 'process-article', () => articleToHtml(id)))
    )
  }


  deleteArticle(id: string, rev: string): Promise<void> {
    return this.withTask('delete-article', (task) => pouchDelete(id, rev)
      .then(() => {
        return
      })
    )
  }


  readArticle(id: string): Promise<Attachment[]> {
    return this.withTask('read-article', (task) => Promise
      .resolve(id)
      .then(id => pouchReadArticle(id))
      .then(fullDoc => {
        const infoAttachment = Promise
          .resolve({ ...fullDoc, _attachments: undefined })
          .then(doc => objectToAttachment(doc, 'article'))

        const fetchAttachments: Promise<Attachment>[] = Object
          .entries(fullDoc._attachments)
          .map(entry => this.withItem(task, `read-article-attachment-${entry[0]}`, () => blobToAttachment(entry[1].data, entry[0])))

        return Promise
          .allSettled([ infoAttachment, ...fetchAttachments])
          .then(results => results.filter(r => r.status === 'fulfilled'))
          .then(results => results.map(r => (r as PromiseFulfilledResult<Attachment>).value))
      })
  )}


  saveArticle(article: Article): Promise<void> {
    return this.withTask('save-article', (task) => Promise
      .resolve(article)
      .then(a => this.withItem(task, 'process-article', () => articleToSaveable(a)))
      .then(file => {
        const a = {
          ...article,
          markdown: String(file),
          links: file.data.links,
          images: file.data.images
        }

        const fetchImages: Promise<DownloadedImage>[] = Object
          .entries(a.images)
          .map(entry => this.withItem(task, `download-image-${entry[0]}`, () => fetchImage(entry[0], entry[1])))

        return Promise
          .allSettled(fetchImages)
          .then(fetchedImages => this.withItem(task, 'persist-article', () => pouchSaveArticle(a, fetchedImages)))
      })
      .then(() => {
        return
      })
    )
  }


  getAttachments(docId: string, attachments: string[]): Promise<Attachment[]> {
    return this.withTask('get-attachments', (task) => Promise
      .resolve()
      .then(() => {
        const fetchAttachments: Promise<Attachment>[] = attachments
          .map(attachmentId => this.withItem(task, `fetch-attachment-${attachmentId}`, () => pouchGetAttachments(docId, attachmentId)
            .then(blob => blobToAttachment(blob, attachmentId))
          ))

        return Promise.allSettled(fetchAttachments)
          .then(results => results
            .filter(r => r.status === 'fulfilled')
            .map(r => (r as PromiseFulfilledResult<Attachment>).value)
          )
      })
    )
  }

}

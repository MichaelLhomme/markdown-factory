import { Controller, initControllerClient, registerController } from './abstract-controller'
import { Entities, PouchDoc, PouchDocId, PouchId, PouchStub, deleteEntitiesMatchingId, deleteEntity, findEntities, findEntity, saveEntity } from '../utils/pouchdb'
import { captureTab, addUrlToCapture, findCapturesByBatch, generateCaptureIdPattern, saveCapture, CaptureDoc } from './capture-controller'
import { closeTab, openTabForCapture } from '../utils/tabs'
import { Events, EventsEmitter, createEventsEmitter, initEventsListener } from './events'


export type BatchId = PouchId

export interface Batch {
  name: string
  urls: string[]
}

export interface BatchComputation {
  externalUrls: string[]
  remaining: string[]
  resolved: Record<string, PouchId>
}

export type BatchExtendedInfo = Batch & BatchComputation & {
  processing: boolean
  acceptedUrls: string[] // TODO store with the Batch
  rejectedUrls: string[] // TODO store with the Batch
}


export type BatchDoc = PouchDoc<Batch>
export type BatchStub = PouchStub<Batch>
export type BatchFull = PouchDoc<BatchExtendedInfo>


export interface BatchController extends Controller {
  listBatches: () => Promise<BatchDoc[]>
  getBatch: (id: BatchId) => Promise<BatchFull>
  deleteBatch: (id: PouchDocId) => Promise<PouchDocId>
  createBatch: (batch: BatchStub) => Promise<BatchDoc>
  saveBatch: (batch: BatchStub) => Promise<BatchDoc>
  startBatch: ([ batchId, urls ]: [ BatchId, string[] ]) => Promise<void>
  stopBatch: (batchId: BatchId) => Promise<void>
}

export interface BatchEvents extends Events {
  newCapture: CaptureDoc
  processingUpdated: boolean
}


let events: EventsEmitter<BatchEvents>
let processing = false

export const findBatches = () => findEntities<BatchDoc>(Entities.BATCH)
export const findBatch = (id: PouchId) => findEntity<BatchDoc>(Entities.BATCH, id)
export const _saveBatch = (batch: BatchStub) => saveEntity(Entities.BATCH, batch)


export const createBatch = (batch: BatchStub): Promise<BatchDoc> => _saveBatch(batch)
  .then(docId => findBatch(docId.id))


export const saveBatch = (batch: BatchStub): Promise<BatchDoc> => _saveBatch(batch)
  .then(res => findBatch(res.id))


export const getBatch = (id: BatchId): Promise<BatchFull> => findBatch(id)
  .then(batch => ({ ...batch, processing }))
  .then(batch => computeState(batch.id)
    .then(res => ({ ...batch, ...res, acceptedUrls: [], rejectedUrls: [] }))
  )


export const deleteBatch = (docId: PouchDocId): Promise<PouchDocId> => {
  const idPattern = generateCaptureIdPattern(docId.id)
  return deleteEntitiesMatchingId(Entities.CAPTURE, idPattern)
    .then(() => deleteEntity(Entities.BATCH, docId))
}


export const startBatch = (batchId: BatchId, urls: string[]): Promise<void> => {
  updateProcessing(true)
  return computeState(batchId)
    .then(res => processUrls(batchId, urls, res.resolved))
}


export const stopBatch = (_batchId: BatchId): Promise<void> => {
  updateProcessing(false)
  return Promise.resolve()
}


const updateProcessing = (p: boolean): void => {
  processing = p
  events.processingUpdated(processing)
}


const cleanupLinks = (links: string[]): string[] => {
  return links
    .filter(l => l.startsWith('http'))
    .map(l => {
      const u = new URL(l)
      return `${u.origin}${u.pathname}${u.search}`
    })
}


const computeState = (batchId: BatchId): Promise<BatchComputation> => findCapturesByBatch(batchId)
  .then(captures => captures
    .reduce((acc, c) => {
      c.urls.forEach(u => acc.resolved[u] = c.id)

      cleanupLinks(c.info.links)
        .forEach(l => acc.externalReferences[l] = true)

      return acc
    }, { resolved: {}, externalReferences: {} } as { resolved: Record<string, PouchId>, externalReferences: Record<string, boolean> })
  )
  .then(res => {
    const resolved = res.resolved
    const externalUrls = Object.keys(res.externalReferences)
    const remaining = externalUrls.filter(u => res.resolved[u] === undefined)

    return {
      externalUrls,
      resolved,
      remaining
    }
  })


const processUrls = (batchId: BatchId, urls: string[], resolved: Record<string, PouchId>): void => {
  if(urls.length === 0 || processing === false) {
    updateProcessing(false)
    return
  }

  const [ url, ...remainingUrls ] = urls
  openTabForCapture(url)
    .then(tab => {
      if(!tab.url) throw new Error('Missing tab url')
      const tabUrl = tab.url

      if(resolved[tabUrl]) {
        console.log('Already resolved url, adding new url to capture')
        return addUrlToCapture(resolved[tabUrl], url)
          .then(() => ({ ...resolved, [url]: resolved[tabUrl] }))
          .finally(() => closeTab(tab.id))
      } else {
        return captureTab(batchId, tab.id)
          .then(capture => saveCapture(capture))
          .then(doc => {
            events.newCapture(doc)
            return { ...resolved, [tab.url as string]: doc.id }
          })
          .finally(() => closeTab(tab.id))
      }
    })
    .then(resolved => processUrls(batchId, remainingUrls, resolved))
    .catch(err => {
      console.warn(`Error while processing ${url}, skipping`, err)
      processUrls(batchId, remainingUrls, resolved)
    })
}


const controller: BatchController = {
  getBatch,
  createBatch,
  saveBatch,
  deleteBatch,
  stopBatch,
  listBatches: findBatches,
  startBatch: ([ batchId, urls ]: [ BatchId, string[] ]) => startBatch(batchId, urls),
}


const CONTROLLER_NAME = 'batch'
export const registerBatchController = () => {
  events = createEventsEmitter<BatchEvents>(CONTROLLER_NAME, {
    newCapture: true,
    processingUpdated: true,
  })

  registerController(CONTROLLER_NAME, controller)
}

export const withBatchBridgeClient = initControllerClient(CONTROLLER_NAME, controller)
export const withBatchEventsListener = initEventsListener(CONTROLLER_NAME)

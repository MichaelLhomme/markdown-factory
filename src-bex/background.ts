import { bexBackground } from 'quasar/wrappers';
import { Controller } from './controllers/controller';
import { setupBridge } from './utils/bridge';
import { registerBatchController } from './controllers/batch-controller';
import { registerCaptureController } from './controllers/capture-controller';

console.log('Restarting background script...')

chrome.runtime.onInstalled.addListener(() => {
  chrome.action.onClicked.addListener((/* tab */) => {
    // Opens our extension in a new browser window.
    // Only if a popup isn't defined in the manifest.
    chrome.tabs.create(
      {
        url: chrome.runtime.getURL('www/index.html'),
      },
      (/* newTab */) => {
        // Tab opened.
      }
    );
  });
});

declare module '@quasar/app-vite' {
  interface BexEventMap {
    /* eslint-disable @typescript-eslint/no-explicit-any */
    log: [{ message: string; data?: any[] }, never];
    getTime: [never, number];

    'storage.get': [{ key: string | null }, any];
    'storage.set': [{ key: string; value: any }, any];
    'storage.remove': [{ key: string }, any];
    /* eslint-enable @typescript-eslint/no-explicit-any */
  }
}

export default bexBackground((bridge /* , allActiveConnections */) => {
  setupBridge(bridge)
  registerBatchController()
  registerCaptureController()
  new Controller(bridge)
})

# TODO

## UI
- pagination
- better filtering
- progress notification
- better use/reuse store api (dialogs should access it without using props)

## Background
- progress notification

## Parsing
- parse hero from HTML HEAD
- remove link for images linking to self
- remove empty link ('' and about:blank)
